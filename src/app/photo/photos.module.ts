import { SearchComponent } from './photo-list/search/search.component';
import { CardModule } from './../shared/components/card/card.module';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { PhotoComponent } from "./photo.component";
import { PhotoListComponent } from './photo-list/photo-list.component';
import { PhotoFormComponent } from './photo-form/photo-form.component';
import { PhotosComponent } from './photo-list/photos/photos.component';
import { filterByDescription } from './photo-list/filter-by-description.pipe';
import { LoadButtonComponent } from './photo-list/load-button/load-button.component';
import { DarkenOnHoverModule } from '../shared/directives/darken-on-hover/darken-on-hover.module';

@NgModule({
    declarations: [ 
        PhotoComponent, 
        PhotoListComponent, 
        PhotoFormComponent, 
        PhotosComponent,
        SearchComponent,
        filterByDescription,
        LoadButtonComponent
    ],
    exports: [ PhotoComponent ],
    imports: [ 
        HttpClientModule, 
        CommonModule,
        CardModule,
        DarkenOnHoverModule
    ]
})

export class PhotosModule {}